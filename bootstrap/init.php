<?php

session_start();
include "constants.php";
include DIRECTORY_PATCH . "bootstrap/config.php";
include DIRECTORY_PATCH . "libs/helpers.php";

try {
    $pdo = new PDO("mysql:dbname=$dbInfo->dbname;host={$dbInfo->hostName}",$dbInfo->user,$dbInfo->pass);
}catch (PDOException $e){
    diePage("connection unsuccessful => ".$e->getMessage());
}


include DIRECTORY_PATCH . "libs/lib-auth.php";
include DIRECTORY_PATCH . "libs/lib-tasks.php";