<?php
if (!defined("DIRECTORY_PATCH")){
    echo "premition denied";
    die();
}

function get_folders(){
    global $pdo;
    $currentUserId = getcurrentUserId();
    $query = "select * from folders where user_id = $currentUserId";
    $stmt = $pdo->prepare($query);
    $stmt ->execute();
    $records = $stmt -> fetchAll(PDO::FETCH_OBJ);
    return $records;
}

function get_folder($folderId){
    global $pdo;
    $query = "select * from folders where id = $folderId";
    $stmt = $pdo->prepare($query);
    $stmt ->execute();
    $records = $stmt -> fetchAll(PDO::FETCH_OBJ);
    return $records;
}
function add_folder($folderName){
    global $pdo;
    $currentUserId = getcurrentUserId();
    $query = "insert into folders(name,user_id) values (:folderName,:currentUserId)";
    $stmt = $pdo->prepare($query);
    $stmt ->execute([':folderName' => $folderName , ':currentUserId' =>$currentUserId]);
    return $pdo ->lastInsertId();

}
function deleteFolder($folderId){
    global $pdo;
    $query = "delete from folders where id=".$folderId;
    $stmt = $pdo -> prepare($query);
    $stmt -> execute();
    return $stmt -> rowCount();
}
function switchDone($taskId){
    global $pdo;
    $currentUserId = getcurrentUserId();
    $query = "update tasks set is_done = 1-is_done where user_id = :currentUserId and id= :taskId";
    $stmt = $pdo->prepare($query);
    $stmt ->execute([':taskId' => $taskId , ':currentUserId' =>$currentUserId]);
    return $stmt ->rowCount();
}
function addTask($title,$folderId){
    global $pdo;
    $currentUserId = getcurrentUserId();
    $query = "insert into tasks(title,user_id,folder_id) values (:title,:currentUserId,:folder_id)";
    $stmt = $pdo->prepare($query);
    $stmt ->execute([':title' => $title , ':currentUserId' =>$currentUserId , ':folder_id' =>$folderId]);
    return $stmt ->rowCount();

}
function deleteTask($taskId){
    global $pdo;
    $query = "delete from tasks where id=".$taskId;
    $stmt = $pdo -> prepare($query);
    $stmt -> execute();
    return $stmt -> rowCount();
}
function get_tasks(){
    global $pdo;
    $folder = isset($_GET['selectFolder']) ? $_GET['selectFolder'] : null;
    if ( isset($folder) and is_numeric($folder)){
        $filterFolder = "and folder_id = ".$folder;
    }
    if (!isset($folder) and !is_numeric($folder)){
        $filterFolder = "";
    }
    $currentUserId = getcurrentUserId();
    $query = "select * from tasks where user_id = $currentUserId $filterFolder";
    $stmt = $pdo->prepare($query);
    $stmt ->execute();
    $records = $stmt -> fetchAll(PDO::FETCH_OBJ);
    return $records;
}
