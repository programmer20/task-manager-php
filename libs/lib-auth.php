<?php
if (!defined("DIRECTORY_PATCH")){
    echo "premition denied";
    die();
}
function getcurrentUserId(){
    return getUserData() ->id ?? 0;
}
function isLogin(){
return isset($_SESSION['login']) ? true : false ;
}
function getUserByEmail($email){
    global $pdo;
    $query = "select * from users where email = :email";
    $stmt = $pdo->prepare($query);
    $stmt ->execute([':email' =>$email]);
    $records = $stmt -> fetchAll(PDO::FETCH_OBJ);
    return $records[0] ?? null;
}
function login($email,$password){
    global $pdo;
    $user = getUserByEmail($email);
    #email validation
    if (is_null($user)){
        return false;
    }
    #check the pass
    if (password_verify($password,$user -> password)){
        $_SESSION['login'] = $user;
        return true;
    }
    return false;

}

function register($name,$email,$password){
    global $pdo;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
         return "Invalid email format";
    }
    $password = password_hash($password, PASSWORD_BCRYPT);
    $query = "insert into users(name,email,password) values (:name,:email,:password)";
    $stmt = $pdo->prepare($query);
    $stmt ->execute([':name' => $name , ':email' =>$email , ':password' =>$password]);
    return $stmt ->rowCount() ? true :false;
}
function getUserData(){
    return $_SESSION['login'] ?? null;
}
function loguot(){
    unset($_SESSION['login']);
}