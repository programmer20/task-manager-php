<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>MainPage</title>
  <link rel="stylesheet" href="assets/Css/style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="page">
  <div class="pageHeader">
    <div class="title">Dashboard</div>
    <div class="userPanel">
        <a href="?logout=1"><i class="fa fa-sign-out"></i></a>
        <span class="username"><?= $userData -> name?></span></div>
  </div>
  <div></div>
  <div class="main">
    <div class="nav">
      <div class="searchbox">
        <div><i class="fa fa-search"></i>
          <input type="search" placeholder="Search"/>
        </div>
      </div>
      <div class="menu">
        <div class="title"> folders</div>
        <ul class="folder-list">
            <a href="<?= DIR_URL?>" style="text-decoration: none; color: #000000">
                <li  class="<?= !isset($_GET['selectFolder']) ? 'active' : ''?>"> <i class=" <?= !isset($_GET['selectFolder']) ? 'fa fa-folder-open' : 'fa fa-folder'?>"></i>All</li>

            </a>

            <?php foreach ($folders as $folder):?>
                <li class="<?= (@$_GET['selectFolder'] == $folder->id) ? 'active' : ''?>">
                    <a href="?selectFolder=<?=  $folder->id?>" >
                        <i class=" <?= (@$_GET['selectFolder'] == $folder->id) ? 'fa fa-folder-open' : 'fa fa-folder'?>"></i><?=  $folder->name?>
                    </a>
                    <a href="?deleteFolder=<?=  $folder->id?>"><i class="fa fa-trash-o" style="float: right ; color: red"></i></a>
                </li>

            <?php endforeach;?>


        </ul>
      </div>
        <div>
            <input type="text" id="newFolderInput" placeholder="folder name" style="padding: 3px; margin-left: 1%; width: 78%; border: 2px solid #c9c9c9; border-radius: 3px; }">
            <button id="addFolderBtn">+</button>
        </div>

    </div>
    <div class="view">
      <div class="viewHeader">
        <div class="title"> new task: <input style="margin-left: 1px ; width: 56%; height: 62%; font-size: 16px; font-family: cursive; padding: 4px ;" type="text" placeholder="new task here..." name="new-task" id="new-task"></div>


        <div class="functions">

          
        </div>
      </div>
      <div class="content">
        <div class="list">
          <ul>
              <?php if(sizeof($tasks)):?>
              <?php foreach ($tasks as $task):?>
                  <li class="<?= ($task -> is_done ? "checked":"check");?>"><i data-taskId="<?= $task -> id?>" class="isDone <?= ($task -> is_done ? "fa fa-check-square-o":"fa fa-square-o");?>"></i><span><?= $task -> title?></span>
                      <div class="info">
                          <span style="margin-right:10px;">created at <?= $task -> created_at?></span>
                          <a href="?deleteTask=<?= $task -> id?>" onclick="return confirm('are you sure?')">
                              <i class="fa fa-trash-o" style="text-decoration: none; color: #ff0000"></i>
                          </a>
                      </div>
                  </li>

              <?php endforeach;?>
              <?php else:?>
                  <li>no task here</li>
              <?php endif;?>


          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- partial -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src='assets/js/script.js'></script>
<script>
    $(document).ready(function(){
        $(".isDone").click(function(e){
            var taskId = $(this).attr("data-taskId");
            $.ajax({
                url : "process/ajax_handler.php",
                method : "post",
                data : {action : "switchDone" , taskId : taskId},
                success : function (response){
                    if (response == 1){
                        location.reload();
                    }else {
                        alert(response);
                    }

                }
            });
        })

        $("#addFolderBtn").click(function(e){
            let input = $("#newFolderInput");
            $.ajax({
                url : "process/ajax_handler.php",
                method : "post",
                data : {action : "addFolder" , folderName : input.val()},
                success : function (response){
                  if (response == 1){
                      location.reload();
                  }
                }
            });
        });
    });
    $("#new-task").on("keypress",function (e){
        if (e.which === 13) {
            $.ajax({
                url : "process/ajax_handler.php",
                method : "post",
                data : {action : "addTask" ,folderId : <?= $_GET['selectFolder'] ?? 0;?>, taskName : $("#new-task").val()},
                success : function (response){
                    if (response == 1){
                        location.reload();
                    }else {
                        alert(response);
                    }

                }
            });
        }
    })
    $("#new-task").focus();

</script>



</body>
</html>
