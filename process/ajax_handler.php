<?php
include_once "../bootstrap/init.php";
if (!isAjax()){
    diePage(" invalid request!");
}

if (!isset($_POST['action']) || empty($_POST['action'])){
    diePage("invalid request");
}
switch ($_POST['action']){
    case "switchDone":
        $taskId = $_POST['taskId'];
        if (!isset($taskId) || !is_numeric($taskId)){
            diePage("تسک وارد شده معتبر نمیباشد!");
        }
        echo 1;
        switchDone($taskId);
        break;
        
        case "addFolder":
        $folderName = $_POST['folderName'];
        $folderId = add_folder($folderName);
        echo 1;
        break;

    case "addTask":
      $title = $_POST['taskName'];
      $folderId = $_POST['folderId'];
      if (empty($folderId)){
          diePage("لطفا ابتدا یک فولدر را انتخاب کنید!");
      }
      if (!isset($title) || strlen($title)<3){
          diePage("طول کاراکتر غیر مجاز!");
      }

      echo addTask($title,$folderId);
        break;

    default:
        break;
}